package az.ingress.com.example.commonms.dto.User;

import jakarta.persistence.Column;
import jakarta.persistence.Transient;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RegisterDto {
    @NotBlank(message = "Xahiş olunur adınızı daxil edin!")
    @Size(min = 3 , message = "Adınız minimum 3 simvol olmalıdır!")
    String name;

    @NotBlank(message = "Xahiş olunur soyadınızı daxil edin!")
    @Size(min = 3, message = "Soyadınız minimum 3 simvol olmalıdır!")
    String surname;

    @Column(unique = true, nullable = false)
    @Size(min = 5, message = "Mail adresiniz minimum 5 simvol olmalıdır!")
    @NotBlank(message = "Xahiş olunur mail adresinizi daxil edin!")
    @Email(message = "Xahiş olunur düzgün bir mail ünvanı daxil edin!")
    String email;

    @Size(min = 7, max = 10, message = "Parol minimum 7, maksimum 10 simvol olmalıdır!")
    @NotBlank(message = "Xahiş olunur parolunuzu daxil edin!")
    String password;

    @Transient
    @NotBlank(message = "Xahiş olunur parolu təkrar daxil edin!")
    String repeatedPassword;

    List<String > authorities;
    String userType;


}
