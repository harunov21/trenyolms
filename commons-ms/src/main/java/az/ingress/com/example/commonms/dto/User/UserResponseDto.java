package az.ingress.com.example.commonms.dto.User;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserResponseDto implements Serializable {
    Long id;
    String name;
    String surname;
    String password;
    String address;
    String email;

}
