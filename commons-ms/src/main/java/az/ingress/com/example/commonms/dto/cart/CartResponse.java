package az.ingress.com.example.commonms.dto.cart;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CartResponse {
    Long id;
    Long customerId;
    @JsonIgnoreProperties("cart")
    List<CartItemResponse> cartItems;
    LocalDateTime createdAt;
    LocalDateTime updatedAt;
}