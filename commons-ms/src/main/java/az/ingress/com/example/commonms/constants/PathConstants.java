package az.ingress.com.example.commonms.constants;

public class PathConstants {
    public static final String API_V1 = "/api/v1";

    public static final String API_V1_PRODUCT = API_V1 + "/products";

    public static final String API_V1_USER = API_V1 + "/users";

    public static final String API_V1_ORDER = API_V1 + "/orders";

    public static final String API_V1_NOTIFICATION = API_V1 + "/notifications";


}
