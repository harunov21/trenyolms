package az.ingress.com.example.commonms.constants;

public class FeignConstants {
    public static final String CATEGORY_SERVICE = "catalog-service";
    public static final String USER_SERVICE = "user-ms";
    public static final String ORDER_SERVICE = "order-ms";
    public static final String CART_SERVICE = "cart-management-service";
    public static final String FAVORITE_SERVICE = "favorite-service";
}
