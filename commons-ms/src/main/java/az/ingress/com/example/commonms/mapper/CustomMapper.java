package az.ingress.com.example.commonms.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CustomMapper {

    @Bean
    public ModelMapper getMapper (){
        return new ModelMapper();
    }
}
