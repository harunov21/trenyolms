package az.ingress.notificationms.Feign;

import az.ingress.userms.Dto.UserResponseDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.FeignClientProperties;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Optional;

import static az.ingress.com.example.commonms.constants.FeignConstants.USER_SERVICE;
import static az.ingress.com.example.commonms.constants.PathConstants.API_V1_USER;

@FeignClient(value = USER_SERVICE , configuration = FeignClientProperties.FeignClientConfiguration.class)
public interface UserMs {
    Optional<UserResponseDto> findById(@PathVariable Long userId,
                                       @RequestHeader("Authorization") String authorizationHeader);

    @GetMapping(API_V1_USER + "/exists")
    Boolean checkUser(@RequestParam Long userId,
                      @RequestHeader("Authorization") String authorizationHeader);
}





