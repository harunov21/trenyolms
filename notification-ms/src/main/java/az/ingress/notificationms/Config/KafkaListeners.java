package az.ingress.notificationms.Config;

import az.ingress.com.example.commonms.dto.User.RegisterDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class KafkaListeners {

    @KafkaListener(topics = "userTopic" , groupId = "groupId")
    void listener (RegisterDto registerDto){
        log.info("listeners received data : {} , ", registerDto );
    }

}
