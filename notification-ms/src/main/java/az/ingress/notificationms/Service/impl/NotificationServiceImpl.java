package az.ingress.notificationms.Service.impl;
import az.ingress.notificationms.Feign.UserMs;
import az.ingress.notificationms.Model.Notification;
import az.ingress.notificationms.Service.NotificationService;
import az.ingress.notificationms.dto.NotificationDto;
import az.ingress.userms.Config.CustomMapper;
import az.ingress.userms.Dto.UserResponseDto;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class NotificationServiceImpl implements NotificationService {
    private final AmqpTemplate amqpTemplate;
    private final UserMs userMs;
    private final JavaMailSender javaMailSender;

    @Value("${notification.queue}")
    private String notificationQueue;

    @Override
    public void sendNotification(NotificationDto notificationDto) {

        // Send AMQP message
        amqpTemplate.convertAndSend(notificationQueue, notificationDto);

        // Send email
        sendEmailNotification(notificationDto);
    }
    private void sendEmailNotification(NotificationDto notificationDto) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(notificationDto.getEmail());
        mailMessage.setSubject("Notification");
        mailMessage.setText(notificationDto.getMessage());

        javaMailSender.send(mailMessage);
    }

    @Override
    public void setEmailForRecipient(NotificationDto notificationDto) {
        Boolean isUserExistResponse = userMs.checkUser(notificationDto.getUsers().getId(), notificationDto.getAuthorizationHeader());

        if (isUserExistResponse != null) {
            Optional<UserResponseDto> userResponse = userMs.findById(notificationDto.getUsers().getId() , notificationDto.getAuthorizationHeader());

            if (userResponse.isPresent()) {
                UserResponseDto user = userResponse.get();
                String userEmail = user.getEmail();
                notificationDto.setEmail(userEmail);
            }
        }
    }
}





