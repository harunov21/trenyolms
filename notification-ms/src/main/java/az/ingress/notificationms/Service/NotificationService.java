package az.ingress.notificationms.Service;

import az.ingress.notificationms.Model.Notification;
import az.ingress.notificationms.dto.NotificationDto;

public interface NotificationService {

     void sendNotification(NotificationDto notificationDto);

     void setEmailForRecipient(NotificationDto notificationDto);
}
