package az.ingress.notificationms.dto;

import az.ingress.userms.Model.Users;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.java.Log;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class NotificationDto {
    Long id;
    String recipient;
    String email;
    String message;
    String authorizationHeader;
    Users users;


}
