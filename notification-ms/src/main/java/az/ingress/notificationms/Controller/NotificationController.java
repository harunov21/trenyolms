package az.ingress.notificationms.Controller;

import az.ingress.notificationms.Model.Notification;
import az.ingress.notificationms.Service.NotificationService;
import az.ingress.notificationms.dto.NotificationDto;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/notifications")
@RestController
@RequiredArgsConstructor
public class NotificationController {
    private final NotificationService notificationService;
    @PostMapping("/sendNotification")
    public void sendNotification(@RequestBody NotificationDto notificationDto) {
        notificationService.setEmailForRecipient(notificationDto);
        notificationService.sendNotification(notificationDto);
    }
}





