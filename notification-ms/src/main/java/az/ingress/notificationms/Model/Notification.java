package az.ingress.notificationms.Model;

import az.ingress.notificationms.Enum.NotificationType;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Notification {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String recipient;
    String email;
    String message;
    String authorizationHeader;

    @Enumerated(EnumType.STRING)
    NotificationType type;
}
