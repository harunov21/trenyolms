
E-Commerce Microservices Project

This project is developed using a microservices architecture for an e-commerce system. Each microservice is responsible for handling specific functionalities such as user management, favorite products, cart management, product, and order processing. The project is built using technologies like Spring Boot, Spring Cloud, and Docker.

Project Architecture
The system consists of the following microservices:

1. User-MS (User Microservice)
Manages user operations such as creating, updating, deleting users, and handling user authentication.

Endpoints:

POST /users/register: Registers a new user
POST /users/login: User login
GET /users/{id}: Fetch user information
Database: PostgreSQL

2. Favorite-MS (Favorite Products Microservice)
Manages users' favorite products, allowing them to add, remove, and list favorite items.

Endpoints:

POST /favorites: Adds a user's favorite product
GET /favorites: Retrieves a user's favorite products
Database: PostgreSQL

3. Cart-MS (Cart Microservice)
Manages user shopping carts, including adding, removing products, and handling the checkout process.

Endpoints:

POST /carts/create: Creates a new cart
GET /carts/{cartId}: Retrieves cart information
PUT /carts/{cartId}/complete: Completes the cart and creates an order
POST /carts/{cartId}/items: Adds a product to the cart
DELETE /carts/{cartId}/items/{itemId}: Removes a product from the cart
Database: PostgreSQL

4. Order-MS (Order Microservice)
Handles the creation of orders from completed carts.

Endpoints:

POST /orders: Creates an order from the cart
GET /orders/{orderId}: Retrieves order details
5. API Gateway
The API Gateway routes incoming requests to the appropriate microservice using Spring Cloud Gateway.

Configuration:

/users/**: Routes to User-MS
/favorites/**: Routes to Favorite-MS
/carts/**: Routes to Cart-MS
/orders/**: Routes to Order-MS
6. Discovery Server (Eureka Server)
Eureka Server is used for service discovery. All microservices register with the server for dynamic service discovery.

Configuration:

Microservices register with Eureka Server and communicate with each other via HTTP requests.
Project Dependencies
Spring Boot 3.1.8
Spring Cloud 2023.0.3
Spring Cloud Config Server
Swagger UI (API documentation)
Spring Cloud Netflix Eureka Server (Service discovery)
Resilience4j (Circuit Breaker)
Feign Client (Inter-service communication)
Spring Data JPA (Database access)
ModelMapper (DTO to entity conversion)
Swagger OpenAPI (API documentation)
Docker (Containerization)
Setup & Installation
Clone the Repository
bash
Copy code
git clone <repository-url>
Running the Project with Docker
Docker containers for each microservice are created and ready. You can start all services at once using Docker Compose.

Run the following command to start all services:

bash
Copy code
docker-compose up --build
Running the Microservices Independently
You can also run each microservice independently:

User-MS: mvn spring-boot:run
Favorite-MS: mvn spring-boot:run
Cart-MS: mvn spring-boot:run
API Gateway: mvn spring-boot:run
Eureka Server: mvn spring-boot:run
Swagger UI
You can access the API documentation using Swagger UI:

API Gateway: http://localhost:8080/swagger-ui.html
Accessing the Microservices
All microservices can be accessed via the API Gateway.

API Documentation
Swagger UI allows you to access the API documentation for all microservices. Each microservice has its own Swagger UI for easy testing and exploration of the available endpoints.

Running Tests
The project includes tests to verify the functionality of the microservices. You can run the tests using the following command:

bash
Copy code
mvn test
Contributing
If you'd like to contribute to this project, you can submit a pull request with your suggestions. Here's how you can contribute:

Fork the repository
Create a feature branch (git checkout -b feature-name)
Make your changes and commit them (git commit -am 'Add new feature')
Push your changes (git push origin feature-name)
Create a pull request
License
This project is licensed under the MIT License. For more details, please refer to the LICENSE file.

