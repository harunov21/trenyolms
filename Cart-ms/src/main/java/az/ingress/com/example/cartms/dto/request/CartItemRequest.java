package az.ingress.com.example.cartms.dto.request;

import az.ingress.com.example.cartms.entity.Cart;
import jakarta.validation.constraints.DecimalMax;
import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CartItemRequest {

     Long productId;
    
    @Min(value = 0, message = "Quantity must be greater than or equal to 0")
    @Max(value = 10_000_000, message = "Quantity must be less than or equal to 10 000 000")
     Integer quantity;

    @DecimalMin(value = "0.00", message = "Price must be greater than or equal to 0.00")
    @DecimalMax(value = "99999999.99", message =  "Price must be less than or equal to 99 999 999.99")
     BigDecimal price;

    Cart cart;

}