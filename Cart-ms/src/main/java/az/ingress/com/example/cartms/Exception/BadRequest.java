package az.ingress.com.example.cartms.Exception;

import az.ingress.com.example.cartms.enums.ErrorFields;
import lombok.Getter;

@Getter
public class BadRequest extends RuntimeException{
    private final ErrorFields errorFields;


    public BadRequest(ErrorFields errorFields) {
        this.errorFields = errorFields;
    }
}
