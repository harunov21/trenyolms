package az.ingress.com.example.cartms.repository;

import az.ingress.com.example.cartms.entity.Cart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CartRepository extends JpaRepository<Cart, Long> {
    void deleteByUserId(Long customerId);
    Cart findByUserId(Long customerId);
}
