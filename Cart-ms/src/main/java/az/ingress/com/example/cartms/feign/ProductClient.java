package az.ingress.com.example.cartms.feign;


import az.ingress.com.example.commonms.dto.product.ProductResponseDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.FeignClientProperties;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import static az.ingress.com.example.commonms.constants.FeignConstants.CATEGORY_SERVICE;
import static az.ingress.com.example.commonms.constants.PathConstants.API_V1_PRODUCT;


@FeignClient(name = CATEGORY_SERVICE , configuration = FeignClientProperties.FeignClientConfiguration.class)
@Component
public interface ProductClient {
    @GetMapping(value=API_V1_PRODUCT + "/{productId}")
    ResponseEntity<ProductResponseDto> getProductById(@PathVariable("productId") Long productId);

}
