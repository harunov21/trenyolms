package az.ingress.com.example.cartms.feign;

import az.ingress.com.example.commonms.dto.User.UserResponseDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.FeignClientProperties;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Optional;

import static az.ingress.com.example.commonms.constants.FeignConstants.USER_SERVICE;
import static az.ingress.com.example.commonms.constants.PathConstants.API_V1_USER;

@Component
@FeignClient(name = "user-ms")
public interface UserClient {
    @GetMapping("/api/v1/users/{userId}")
    Boolean findById(@PathVariable Long userId, @RequestHeader("Authorization") String authorizationHeader);
}


