package az.ingress.com.example.cartms.service;

import az.ingress.com.example.commonms.dto.cart.CartRequest;
import az.ingress.com.example.cartms.dto.request.CartItemRequest;
import az.ingress.com.example.cartms.entity.Cart;

public interface CartService {
    Long create(CartRequest cartRequest ,Long userId ,String authorizationHeader);
    Cart addItem(Long cartId, CartItemRequest cartItemRequest);
    Cart removeItem(Long cartId, Long cartItemId);
    Cart completeCart(Long id);
    Cart getByUserId(Long id);

   Cart getById(Long cartId);
}
