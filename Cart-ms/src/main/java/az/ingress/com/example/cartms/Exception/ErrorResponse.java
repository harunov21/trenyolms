package az.ingress.com.example.cartms.Exception;

import java.time.LocalDateTime;
import java.util.Map;

public class ErrorResponse {
    Map<String ,String > message;
    String path;
    LocalDateTime iat;
    int statusCode;
}
