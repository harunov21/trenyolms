package az.ingress.com.example.cartms.feign;

import az.ingress.com.example.commonms.dto.cart.CartResponse;
import jakarta.validation.Valid;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.FeignClientProperties;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import static az.ingress.com.example.commonms.constants.FeignConstants.ORDER_SERVICE;
import static az.ingress.com.example.commonms.constants.PathConstants.API_V1_ORDER;
@FeignClient(name = ORDER_SERVICE, configuration = FeignClientProperties.FeignClientConfiguration.class)
@Component
public interface OrderClient {
    @PostMapping(value=API_V1_ORDER)
    ResponseEntity<Object> create(@RequestBody CartResponse cart);
}
