package az.ingress.com.example.cartms.entity;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CartItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
     Long id;

    @Column(name = "product_id", nullable = false)
     Long productId;

    @Column(name = "quantity", columnDefinition="Integer default 0", nullable = false)
     Integer quantity = 0;

    @Column(name = "price", columnDefinition="Decimal(10,2) default '0.00'", nullable = false)
     BigDecimal price = BigDecimal.valueOf(0);

    @ManyToOne
    @JoinColumn(name = "cart_id", nullable = false)
    Cart cart;

    @CreationTimestamp
    @Column(name = "createdAt")
     LocalDateTime createdAt;

    @UpdateTimestamp
    @Column(name = "updatedAt")
     LocalDateTime updatedAt;

}
