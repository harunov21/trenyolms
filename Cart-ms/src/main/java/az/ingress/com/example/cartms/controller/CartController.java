package az.ingress.com.example.cartms.controller;

import az.ingress.com.example.commonms.dto.cart.CartRequest;
import az.ingress.com.example.cartms.dto.request.CartItemRequest;
import az.ingress.com.example.cartms.entity.Cart;
import az.ingress.com.example.cartms.service.CartService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
//import springfox.documentation.annotations.ApiIgnore;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequiredArgsConstructor
@RequestMapping("/carts")
public class CartController {

    private final CartService cartService;


    @PostMapping("/create{userId}")
    public ResponseEntity<Long> createCart(@RequestBody CartRequest cartRequest,
                                           @PathVariable Long userId,
                                           @RequestHeader("Authorization") String authorizationHeader) {
        Long cartId = cartService.create(cartRequest, userId, authorizationHeader);
        return ResponseEntity.ok(cartId);
    }

    @GetMapping("/{cartId}")
    @Operation(summary = "Get cart by ID", description = "Returns a single cart identified by its ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully retrieved cart"),
            @ApiResponse(responseCode = "404", description = "cart not found"),
            @ApiResponse(responseCode = "500", description = "Internal server error")
    })

    public ResponseEntity<Cart> getById(@PathVariable("cartId") Long cartId) {
        return new ResponseEntity<>(cartService.getById(cartId),OK);
    }

    @PutMapping("/{cartId}/complete")
    @Operation(summary = "Create order", description = "Clear customer cart and create order")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully created order"),
            @ApiResponse(responseCode = "404", description = "Cart not found"),
            @ApiResponse(responseCode = "500", description = "Internal server error")
    })
    public ResponseEntity<Cart> completeCart(@PathVariable("cartId") Long cartId) {
        return new ResponseEntity<>(cartService.completeCart(cartId) , OK);
    }

    @GetMapping("/customer/{customerId}")
    @Operation(summary = "Get a cart by customer id", description = "Get a cart by customer id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully retrieved cart"),
            @ApiResponse(responseCode = "404", description = "Customer not found"),
            @ApiResponse(responseCode = "500", description = "Internal server error")
    })
    public ResponseEntity<Cart> getCartByUserId(@PathVariable("customerId") Long userId) {
        return new ResponseEntity<>(cartService.getByUserId(userId) , OK);
    }

    @PostMapping("/{cartId}/items")
    @Operation(summary = "Create a new cart item", description = "Returns a single cart identified by customer id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully created cart item"),
            @ApiResponse(responseCode = "500", description = "Internal server error occurred")
    })
    public ResponseEntity<Cart> createCartItem(@PathVariable("cartId") Long cartId, @Valid @RequestBody CartItemRequest cartItemRequest) {
        Cart cart = cartService.addItem(cartId, cartItemRequest);
        return new ResponseEntity<>(cart, HttpStatus.CREATED);
    }

    @DeleteMapping("/{cartId}/items/{itemId}")
    @Operation(summary = "Delete cart item", description = "Delete cart item")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully deleted cart item"),
            @ApiResponse(responseCode = "500", description = "Internal server error occurred")
    })
    public ResponseEntity<Cart> deleteCartItem(@PathVariable("cartId") Long cartId, @PathVariable("itemId") Long itemId) {
        return new ResponseEntity<>(cartService.removeItem(cartId, itemId) , OK);
    }
}
