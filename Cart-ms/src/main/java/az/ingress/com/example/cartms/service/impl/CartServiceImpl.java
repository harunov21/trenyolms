package az.ingress.com.example.cartms.service.impl;

import az.ingress.com.example.commonms.dto.User.UserResponseDto;
import az.ingress.com.example.commonms.dto.cart.CartRequest;
import az.ingress.com.example.commonms.dto.cart.CartResponse;
import az.ingress.com.example.commonms.dto.product.ProductResponseDto;
import az.ingress.com.example.cartms.dto.request.CartItemRequest;
import az.ingress.com.example.cartms.entity.Cart;
import az.ingress.com.example.cartms.entity.CartItem;
import az.ingress.com.example.commonms.exceptions.AlreadyExistException;
import az.ingress.com.example.commonms.exceptions.NotFoundException;
import az.ingress.com.example.cartms.feign.OrderClient;
import az.ingress.com.example.cartms.feign.ProductClient;
import az.ingress.com.example.cartms.feign.UserClient;
import az.ingress.com.example.commonms.mapper.BasicMapper;
import az.ingress.com.example.cartms.repository.CartItemRepository;
import az.ingress.com.example.cartms.repository.CartRepository;
import az.ingress.com.example.cartms.service.CartService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class CartServiceImpl implements CartService {
    private final CartRepository cartRepository;
    private final UserClient userClient;
    private final ModelMapper mapper;
    private final ProductClient productClient;
    private final CartItemRepository cartItemRepository;
    private final OrderClient orderClient;
    private final BasicMapper basicMapper;

    @Override
    @Transactional
    public Long create(CartRequest cartRequest, Long userId, String authorizationHeader) {
        Boolean userResponse = userClient.findById(userId, authorizationHeader);

        log.info("User details received: {}", userResponse);

        Cart mapped = mapper.map(cartRequest, Cart.class);
        Cart savedCart = cartRepository.save(mapped);

        if (savedCart.getId() == null) {
            log.error("Cart save operation failed");
            throw new RuntimeException("Cart save operation failed");
        }

        return savedCart.getId();
    }


    @Override
    public Cart addItem(Long cartId, CartItemRequest cartItem) {
        Cart cart = getById(cartId);
        ProductResponseDto productResponse = productClient.getProductById(cartItem.getProductId()).getBody();
        Optional<CartItem> existingItem = cart.getCartItems().stream()
                .filter(cartItem::equals)
                .findFirst();

        existingItem.ifPresent(item -> {
            item.setQuantity(item.getQuantity() + cartItem.getQuantity());
            assert productResponse != null;
            item.setPrice(calculatePrice(productResponse.getPrice(), cartItem.getQuantity()));
            cartItemRepository.save(item);
        });

        existingItem.orElseGet(() -> {
            CartItem newCartItem = new CartItem();
            newCartItem.setCart(cart);
            newCartItem.setQuantity(cartItem.getQuantity());
            assert productResponse != null;
            newCartItem.setPrice(calculatePrice(productResponse.getPrice(), cartItem.getQuantity()));
            cart.getCartItems().add(newCartItem);
            cartItemRepository.save(newCartItem);
            return newCartItem;
        });

        return cartRepository.save(cart);
    }

    @Override
    public Cart removeItem(Long cartId, Long itemId) {
        Cart cart = getById(cartId);
        cart.getCartItems().removeIf(item -> item.getId().equals(itemId));
        cartItemRepository.deleteById(itemId);
        return cartRepository.save(cart);
    }

    private BigDecimal calculatePrice(BigDecimal price, int quantity) {
        return price.multiply(BigDecimal.valueOf(quantity));
    }

    @Override
    public Cart getById(Long id) {
        return cartRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Cart not found"));
    }

    @Override
    @Transactional
    public Cart completeCart(Long id) {
        Cart cart = getById(id);
        orderClient.create(basicMapper.convertTo(cart, CartResponse.class));
        //        Clear cart items
        cartItemRepository.deleteAllByCartId(id);
        cart.setCartItems(new ArrayList<>());
        return cart;
    }

    @Override
    public Cart getByUserId(Long id) {
        return cartRepository.findByUserId(id);
    }
}