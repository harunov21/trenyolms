package az.ingress.favoritems.repository;

import az.ingress.favoritems.Model.Favorite;
import az.ingress.favoritems.dto.FavoriteResponseDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FavoriteRepo extends JpaRepository<Favorite , Long> {
    FavoriteResponseDto findByUserId(Long userId);

    Favorite getFavoritesByUserId(Long userId);
}
