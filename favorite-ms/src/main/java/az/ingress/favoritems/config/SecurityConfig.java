package az.ingress.favoritems.config;

import az.ingress.auth.config.GeneralConfig;
import az.ingress.auth.config.JwtService;
import az.ingress.auth.config.JwtTokenService;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.SecurityFilterChain;

@EnableWebSecurity
@Configuration
@Import({JwtService.class , JwtTokenService.class})
public class SecurityConfig extends GeneralConfig {

    public SecurityConfig(UserDetailsService userDetailsService, JwtTokenService jwtTokenService) {
        super(userDetailsService, jwtTokenService);
    }

    @Override
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http.authorizeHttpRequests(auth -> auth
                .requestMatchers("favorite/**").authenticated());
        return super.securityFilterChain(http);
    }
}
