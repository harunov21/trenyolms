package az.ingress.favoritems.service.impl;

import az.ingress.favoritems.Model.Favorite;
import az.ingress.favoritems.dto.FavoriteRequestDto;
import az.ingress.favoritems.dto.FavoriteResponseDto;
import az.ingress.favoritems.repository.FavoriteRepo;
import az.ingress.favoritems.service.FavoriteService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
@RequiredArgsConstructor
public class FavoriteServiceImpl implements FavoriteService {
    private final FavoriteRepo favoriteRepo;

    @Override
    public FavoriteRequestDto addFavorite(Long userId, Long productId) {
        Favorite favorite = favoriteRepo.getFavoritesByUserId(userId);
        if (favorite == null) {
            favorite = new Favorite();
            favorite.setUserId(userId);
        }

        if (!favorite.getProductIds().contains(productId)) {
            favorite.getProductIds().add(productId);
        }

        favorite = favoriteRepo.save(favorite);

        FavoriteRequestDto favoriteRequestDto = new FavoriteRequestDto();
        favoriteRequestDto.setUserId(favorite.getUserId());
        favoriteRequestDto.setProductIds(new ArrayList<>(favorite.getProductIds()));

        return favoriteRequestDto;
    }
    
    @Override
    public FavoriteResponseDto getFavoriteByUserId(Long userId) {
        return favoriteRepo.findByUserId(userId);

    }
}
