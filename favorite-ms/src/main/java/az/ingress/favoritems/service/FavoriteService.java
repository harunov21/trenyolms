package az.ingress.favoritems.service;

import az.ingress.favoritems.dto.FavoriteRequestDto;
import az.ingress.favoritems.dto.FavoriteResponseDto;

public interface FavoriteService {
    FavoriteRequestDto addFavorite(Long userId,Long productId);

    FavoriteResponseDto getFavoriteByUserId(Long userId);
}
