package az.ingress.favoritems;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FavoriteMsApplication {

    public static void main(String[] args) {
        SpringApplication.run(FavoriteMsApplication.class, args);
    }

}
