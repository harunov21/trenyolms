package az.ingress.favoritems.contrtoller;

import az.ingress.favoritems.dto.FavoriteRequestDto;
import az.ingress.favoritems.dto.FavoriteResponseDto;
import az.ingress.favoritems.service.FavoriteService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("Favorite")
@RequiredArgsConstructor
public class FavoriteController {
    private final FavoriteService favoriteService;

    @Operation(summary = "Add favorite", description = "Add a product to the user's favorites.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Favorite added successfully"),
            @ApiResponse(responseCode = "400", description = "Invalid request format"),
            @ApiResponse(responseCode = "500", description = "Internal server error occurred")
    })
    @PostMapping
    public ResponseEntity<FavoriteRequestDto> addFavorite(
            @Parameter(description = "User ID", required = true)
            @RequestParam Long userId,
            @Parameter(description = "Product ID", required = true)
            @RequestParam Long productId) {
        return new ResponseEntity<>(favoriteService.addFavorite(userId, productId), CREATED);
    }

    @Operation(summary = "Get user's favorites", description = "Get a list of user's favorite products.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully retrieved the user's favorites"),
            @ApiResponse(responseCode = "404", description = "Favorites not found for the user"),
            @ApiResponse(responseCode = "500", description = "Internal server error occurred")
    })
    @GetMapping
    public ResponseEntity<FavoriteResponseDto> getFavoriteByUserId(
            @Parameter(description = "User ID", required = true)
            @RequestParam Long userId) {
        return new ResponseEntity<>(favoriteService.getFavoriteByUserId(userId), OK);
    }
}
