package az.ingress.orderms.Controller;

import az.ingress.orderms.Service.OrderService;
import az.ingress.orderms.dto.Request.OrderRequestDto;
import az.ingress.orderms.dto.Response.OrderResponseDto;

import io.swagger.v3.oas.annotations.Parameter;
import lombok.RequiredArgsConstructor;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;


@RestController
@RequestMapping("/api/v1/orders")
@RequiredArgsConstructor
public class OrderController {
    private final OrderService orderService;
    @Operation(summary = "Create an order", description = "Create a new order.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Order successfully created"),
            @ApiResponse(responseCode = "400", description = "Invalid request format"),
            @ApiResponse(responseCode = "500", description = "Internal server error occurred")
    })
    @PostMapping
    public ResponseEntity<Long> createOrder(@RequestBody OrderRequestDto orderRequestDto) {
        return new ResponseEntity<>(orderService.createOrder(orderRequestDto), CREATED);
    }

    @Operation(summary = "Get all orders", description = "Get a list of all orders with pagination.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully retrieved the orders"),
            @ApiResponse(responseCode = "500", description = "Internal server error occurred")
    })
    @GetMapping
    public ResponseEntity<Page<OrderResponseDto>> getAllOrder(Pageable pageable) {
        return new ResponseEntity<>(orderService.getAll(pageable), OK);
    }

    @Operation(summary = "Find order by ID", description = "Get details of an order by ID.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully retrieved the order"),
            @ApiResponse(responseCode = "404", description = "Order not found"),
            @ApiResponse(responseCode = "500", description = "Internal server error occurred")
    })
    @GetMapping("{id}")
    public ResponseEntity<OrderResponseDto> findById(
            @Parameter(description = "ID of the order to be retrieved", required = true)
            @PathVariable Long id) {
        return new ResponseEntity<>(orderService.findById(id), OK);
    }
}




