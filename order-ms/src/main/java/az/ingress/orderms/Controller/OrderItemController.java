package az.ingress.orderms.Controller;

import az.ingress.orderms.Service.OrderItemService;
import az.ingress.orderms.dto.Request.AddToItemRequestDto;
import az.ingress.orderms.dto.Response.OrderItemResponseDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("/api/v1/orderItems")
@RequiredArgsConstructor
public class OrderItemController {

    private final OrderItemService orderItemService;

    @Operation(summary = "Add item to order", description = "Add an item to the order.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Item added successfully"),
            @ApiResponse(responseCode = "400", description = "User not found or invalid request"),
            @ApiResponse(responseCode = "500", description = "Internal server error occurred")
    })
    @PostMapping("/addToItem")
    public ResponseEntity<String> addToItem(
            @RequestBody AddToItemRequestDto request,
            @RequestHeader("Authorization") String authorizationHeader){
        return new ResponseEntity<>(orderItemService.addToItem(request ,authorizationHeader) , OK);
    }


    @Operation(summary = "Create order item", description = "Create a new order item.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Order item successfully created"),
            @ApiResponse(responseCode = "400", description = "Invalid request format"),
            @ApiResponse(responseCode = "500", description = "Internal server error occurred")
    })
    @PostMapping
    public ResponseEntity<Long> createOrderItem(
            @RequestBody AddToItemRequestDto addToItemRequestDto) {
        return new ResponseEntity<>(orderItemService.createOrderItem(addToItemRequestDto), CREATED);
    }

    @Operation(summary = "Get all order items", description = "Get a list of all order items with pagination.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully retrieved the order items"),
            @ApiResponse(responseCode = "500", description = "Internal server error occurred")
    })
    @GetMapping
    public ResponseEntity<Page<OrderItemResponseDto>> getAll(Pageable pageable) {
        return new ResponseEntity<>(orderItemService.getAll(pageable), OK);
    }

    @Operation(summary = "Find order item by ID", description = "Get details of an order item by ID.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully retrieved the order item"),
            @ApiResponse(responseCode = "404", description = "Order item not found"),
            @ApiResponse(responseCode = "500", description = "Internal server error occurred")
    })
    @GetMapping("/{id}")
    public Optional<OrderItemResponseDto> findById(
            @Parameter(description = "ID of the order item to be retrieved", required = true)
            @PathVariable Long id) {
        return orderItemService.findById(id);
    }
}