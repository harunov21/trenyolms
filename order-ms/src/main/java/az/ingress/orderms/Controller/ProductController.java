package az.ingress.orderms.Controller;

import az.ingress.orderms.Service.ProductService;
import az.ingress.orderms.dto.Request.ProductRequestDto;
import az.ingress.orderms.dto.Response.ProductResponseDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

import static org.springframework.http.HttpStatus.OK;


@RestController
@RequestMapping("/api/v1/product")
@RequiredArgsConstructor
public class ProductController {
    private final ProductService productService;

    @PostMapping
    @Operation(summary = "Crete a Product" , description = "Create a new Product")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200" , description = "Product successfully created "),
            @ApiResponse(responseCode = "400" , description = "Invalid request format"),
            @ApiResponse(responseCode = "500" , description = "Internal server error occurred")
    })
    public ResponseEntity<Long> createProduct(@RequestBody ProductRequestDto productRequestDto) {
        Long productId = productService.create(productRequestDto);
        return new ResponseEntity<>(productId, HttpStatus.CREATED);
    }

    @GetMapping
    @Operation(summary = "Get Product by id" , description = "Getting Product by Product ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200" , description = "Successfully retrieved the Product"),
            @ApiResponse(responseCode = "404", description = "Product not found"),
            @ApiResponse(responseCode = "500", description = "Internal server error occurred")

    })
    public Optional<ProductResponseDto>findById(@RequestParam Long id){
       return productService.findById(id);
    }
}
