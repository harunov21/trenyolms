package az.ingress.orderms.Controller;

import az.ingress.orderms.Model.Category;
import az.ingress.orderms.Service.CategoryService;
import az.ingress.orderms.dto.Request.CategoryRequest;
import az.ingress.orderms.dto.Response.CategoryResponse;
import az.ingress.orderms.dto.Response.ProductResponseDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.Optional;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/categories")
public class CategoryController {
    private final CategoryService categoryService;

    @GetMapping
    @Operation(summary = "Get All Categories", description = "Retrieve all categories")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully retrieved list of categories"),
            @ApiResponse(responseCode = "500", description = "Internal server error occurred")
    })
    public ResponseEntity<Page<CategoryRequest>> getAllCategory (Pageable pageable){
        return new ResponseEntity<>(categoryService.getAll(pageable) , OK);
    }

    @PostMapping("/create")
    @Operation(summary = "Create Category" , description = "Create new Category")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200" , description = "Category Created"),
            @ApiResponse(responseCode = "400" , description = "Invalid input data"),
            @ApiResponse(responseCode = "500" , description = "Internal server error occurred")
    })
    public ResponseEntity<Category> createCategory(@RequestBody Category category){
        return new ResponseEntity<>(categoryService.createCategory(category) , CREATED);
    }


    @GetMapping("/{id}")
    @Operation(summary = "Get category by id" , description = "Getting category by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200" , description = "Successfully retrieved the category"),
            @ApiResponse(responseCode = "404" , description = "Category not found"),
            @ApiResponse(responseCode = "500" , description = "Internal server error occurred")
    })
    public ResponseEntity<Optional<CategoryResponse>> getById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(categoryService.getById(id));
    }

    @Operation(summary = "Get products by category ID", description = "Get a list of products by category ID with pagination.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully retrieved the category"),
            @ApiResponse(responseCode = "404", description = "Category not found"),
            @ApiResponse(responseCode = "500", description = "Internal server error occurred")
    })
    @GetMapping("/{id}/products")
    public ResponseEntity<Optional<ProductResponseDto>> getProductsByCategoryId(@PathVariable("id") Long id, Pageable pageable) {
        return new ResponseEntity<>(categoryService.getProductsByCategoryId(id, pageable) , OK);
    }

    @Operation(summary = "Delete a category", description = "Delete a category by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Category successfully deleted"),
            @ApiResponse(responseCode = "404", description = "Category not found"),
            @ApiResponse(responseCode = "500", description = "Internal server error occurred")
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteCategory(@PathVariable("id") Long id) {
        categoryService.deleteCategory(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }


}
