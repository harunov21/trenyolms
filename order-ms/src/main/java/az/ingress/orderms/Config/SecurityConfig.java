package az.ingress.orderms.Config;

import az.ingress.auth.config.GeneralConfig;
import az.ingress.auth.config.JwtService;
import az.ingress.auth.config.JwtTokenService;
import az.ingress.userms.Service.impl.UserDetailsServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.SecurityFilterChain;


@Configuration
@EnableWebSecurity
@Import({JwtTokenService.class, JwtService.class})
public class SecurityConfig extends GeneralConfig {


    public SecurityConfig(UserDetailsService userDetailsService, JwtTokenService jwtTokenService) {
        super(userDetailsService, jwtTokenService);
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.authorizeHttpRequests(auth -> auth
                .requestMatchers("product/**").authenticated()
                .requestMatchers("categories/**").authenticated()
                .requestMatchers("orderItems/**").authenticated()
        );
        return super.securityFilterChain(http);

    }
}


