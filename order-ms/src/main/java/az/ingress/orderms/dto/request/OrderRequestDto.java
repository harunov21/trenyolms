package az.ingress.orderms.dto.Request;

import az.ingress.orderms.Enum.OrderStatus;
import az.ingress.orderms.Model.OrderItem;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OrderRequestDto implements Serializable {
    BigDecimal totalPrice;
    OrderStatus orderStatus;
    List<OrderItem> orderItems;
    Long productId;
    String orderDescription;


}
