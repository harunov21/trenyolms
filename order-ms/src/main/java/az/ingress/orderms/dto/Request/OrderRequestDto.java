package az.ingress.orderms.dto.Request;

import az.ingress.orderms.Model.OrderItem;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OrderRequestDto implements Serializable {
    double totalPrice;
    String orderStatus;
    List<OrderItem> orderItems;


}
