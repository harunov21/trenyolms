package az.ingress.orderms.dto.Response;


import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.usertype.UserType;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserResponseDto implements Serializable {
    Long id;
    String name;
    String surname;
    String password;
    String address;
    String email;

}
