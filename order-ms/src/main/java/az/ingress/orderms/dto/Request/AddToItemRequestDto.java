package az.ingress.orderms.dto.Request;

import az.ingress.orderms.Model.Order;
import az.ingress.orderms.Model.Product;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AddToItemRequestDto {
    Long id;
    Long userId;
    Product productId;
    String authorizationHeader;
    Integer quantity;
    double price;
    Long orderId;

}
