package az.ingress.orderms.dto.Response;

import az.ingress.orderms.Enum.OrderStatus;
import az.ingress.orderms.Model.OrderItem;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OrderResponseDto implements Serializable {

    BigDecimal totalPrice;
    OrderStatus orderStatus;
    List<OrderItem> orderItems;
}
