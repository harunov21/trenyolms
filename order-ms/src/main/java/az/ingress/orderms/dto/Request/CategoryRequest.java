package az.ingress.orderms.dto.Request;

import az.ingress.orderms.Model.Category;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CategoryRequest implements Serializable {
    @Size(min = 3, max = 25, message = "Name must be between 3 and 30 characters")
    @NotBlank(message = "Name value mustn't be null or whitespace")
     String name;
     Long parentCategoryId;
}