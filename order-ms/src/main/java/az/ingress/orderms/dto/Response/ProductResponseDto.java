package az.ingress.orderms.dto.Response;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ProductResponseDto implements Serializable {
    String name;
    String description;
    BigDecimal price;
    Long categoryId;
}
