package az.ingress.orderms.dto.Response;

import az.ingress.orderms.Model.Order;
import az.ingress.orderms.Model.Product;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OrderItemResponseDto implements Serializable {

    Integer quantity;
    BigDecimal price;
    Order orderId;
    Product productId;
}
