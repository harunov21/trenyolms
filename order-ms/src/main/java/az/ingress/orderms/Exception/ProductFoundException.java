package az.ingress.orderms.Exception;

public class ProductFoundException extends RuntimeException {
    public ProductFoundException(String message) {
        super(message);
    }
}
