package az.ingress.orderms.Exception;

public class OperationException extends RuntimeException{
    public OperationException(String message) {
        super(message);
    }
}
