package az.ingress.orderms.Exception;

import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static org.springframework.http.HttpStatus.FOUND;
import static org.springframework.http.HttpStatus.NOT_FOUND;


@ControllerAdvice
@RequiredArgsConstructor
public class GlobalExceptionHandler {
    private final MessageSource messageSource;

    @ExceptionHandler(BadRequest.class)
    public ResponseEntity<ErrorResponse> exceptionHandler(BadRequest badRequest , WebRequest webRequest) {
        Map<String, String> errorFields = new HashMap();
        errorFields.put("message", messageSource.getMessage(badRequest.getErrorFields().name(), null, Locale.getDefault()));
        ErrorResponse errorResponse = ErrorResponse.builder()
                .iat(LocalDateTime.now())
                .message(errorFields)
                .statusCode(400)
                .build();
        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);

    }

    @ExceptionHandler(ProductNotFoundException.class)
    public ResponseEntity<Map<String,String>> productNotFound(ProductNotFoundException productNotFoundException){
        Map<String ,String> error = new HashMap<>();
        error.put("error message" , productNotFoundException.getLocalizedMessage());
        return new ResponseEntity<>(error , NOT_FOUND);
    }

    @ExceptionHandler(OperationException.class)
    public ResponseEntity<Map<String,String>> operationException(OperationException operationException){
        Map<String,String> error = new HashMap<>();
        error.put("error message" , operationException.getLocalizedMessage());
        return new ResponseEntity<>(error , NOT_FOUND);
    }

    @ExceptionHandler(OrderNotFoundException.class)
    public ResponseEntity<Map<String,String>> orderNotFoundException(OrderNotFoundException orderNotFoundException){
        Map<String,String> error = new HashMap<>();
        error.put("error message" , orderNotFoundException.getLocalizedMessage());
        return new ResponseEntity<>(error,NOT_FOUND);

    }
    @ExceptionHandler(ProductFoundException.class)
    public ResponseEntity<Map<String,String>>  productFoundException(ProductFoundException productFoundException){
        Map<String,String> error = new HashMap<>();
        error.put("error message" , productFoundException.getLocalizedMessage());
        return new ResponseEntity<>(error,FOUND);
    }
    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<Map<String ,String >>userNotFoundException(UserNotFoundException userNotFoundException){
        Map<String ,String > error = new HashMap<>();
        error.put("error message" , userNotFoundException.getLocalizedMessage());
        return new ResponseEntity<>(error,FOUND);


    }
}
