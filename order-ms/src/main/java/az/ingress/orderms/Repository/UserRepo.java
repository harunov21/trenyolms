package az.ingress.orderms.Repository;

import az.ingress.orderms.Model.Users;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepo extends JpaRepository <Users,Long> {
   @Query("select u from Users u where" +
           "(:name is null or u.name=:name)" +
           " and (:surname is null or u.surname=:surname)" +
           " and (:email is null or u.email = :email)"
           )
   Page<Users> findWithSpec(String name, String surname, String email, Pageable pageable);

    Optional<Users> findByName(String username);
}
