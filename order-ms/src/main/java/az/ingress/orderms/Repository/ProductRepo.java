package az.ingress.orderms.Repository;

import az.ingress.orderms.Model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepo extends JpaRepository<Product, Long> {
    Product findBySlug(String slug);

    Product findByName(String productName);
}
