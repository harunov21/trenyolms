package az.ingress.orderms.Repository;

import az.ingress.orderms.Model.Order;
import lombok.extern.java.Log;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface OrderRepo extends JpaRepository<Order,Long> {
}
