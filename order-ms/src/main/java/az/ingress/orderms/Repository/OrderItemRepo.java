package az.ingress.orderms.Repository;

import az.ingress.orderms.Model.OrderItem;
import az.ingress.orderms.Model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface OrderItemRepo extends JpaRepository<OrderItem,Long> {
    Optional<OrderItem> findByOrder_IdAndProduct_Id(Long orderId, Long productId);

    boolean existsByOrderId(Long orderId);
}
