package az.ingress.orderms.Model;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.time.LocalDateTime;

@Entity
@Table(name = "reviews")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Review {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
     Long id;

    @Column(name = "customer_id", nullable = false)
     Long customerId;

    @Column(name = "text")
     String text;

    @ManyToOne
    @JoinColumn(name = "product_id", nullable = false)
     Product product;

    @Column(name = "rating", nullable = false)
     Short rating;

    @CreationTimestamp
    @Column(name = "createdAt")
     LocalDateTime createdAt;

    @UpdateTimestamp
    @Column(name = "updatedAt")
     LocalDateTime updatedAt;
}
