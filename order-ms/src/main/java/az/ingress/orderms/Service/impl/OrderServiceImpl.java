package az.ingress.orderms.Service.impl;

import az.ingress.orderms.Enum.OrderStatus;
import az.ingress.orderms.Exception.OrderNotFoundException;
import az.ingress.orderms.Exception.ProductNotFoundException;
import az.ingress.orderms.Model.Order;
import az.ingress.orderms.Model.OrderItem;
import az.ingress.orderms.Model.Product;
import az.ingress.orderms.Repository.OrderRepo;
import az.ingress.orderms.Repository.ProductRepo;
import az.ingress.orderms.Service.OrderService;
import az.ingress.orderms.dto.Request.OrderRequestDto;
import az.ingress.orderms.dto.Response.OrderResponseDto;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {
    private final OrderRepo orderRepo;
    private final ProductRepo productRepo;

    @Override
    public Long createOrder(OrderRequestDto orderRequestDto) {
        Order orderEntity = new Order();
        orderEntity.setOrderStatus(OrderStatus.PENDING);
        orderEntity.setOrderDescription(orderRequestDto.getOrderDescription());
        orderEntity.setOrderItems(orderRequestDto.getOrderItems());
        orderEntity.setCreatedAt(LocalDate.now());

         BigDecimal totalPrice = BigDecimal.ZERO;
        if (orderRequestDto.getOrderItems() != null) {
            totalPrice = orderRequestDto.getOrderItems().stream()
                    .map(OrderItem::getPrice)
                    .reduce(BigDecimal.ZERO, BigDecimal::add);
        }
        orderEntity.setTotalPrice(totalPrice);

        orderRepo.save(orderEntity);

        return orderEntity.getId();
    }

    @Override
    public OrderResponseDto findById(Long id) {
        Order order = orderRepo.findById(id)
                .orElseThrow(() -> new OrderNotFoundException("Order not found with id: " + id));

        OrderResponseDto orderResponseDto = new OrderResponseDto();
        orderResponseDto.setOrderItems(order.getOrderItems());
        orderResponseDto.setOrderStatus(order.getOrderStatus());
        orderResponseDto.setTotalPrice(order.getTotalPrice());

        return orderResponseDto;
    }

    @Override
    @Cacheable(value = "cacheOrder", key = "'getAllOrders_' + #pageable.pageNumber")
    public Page<OrderResponseDto> getAll(Pageable pageable) {
        Page<Order> orders = orderRepo.findAll(pageable);
        return orders.map(order -> OrderResponseDto.builder()
                .orderItems(order.getOrderItems())
                .orderStatus(order.getOrderStatus())
                .totalPrice(order.getTotalPrice())
                .build());
    }
}
