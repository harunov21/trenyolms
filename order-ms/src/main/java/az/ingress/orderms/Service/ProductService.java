package az.ingress.orderms.Service;

import az.ingress.orderms.dto.Request.ProductRequestDto;
import az.ingress.orderms.dto.Response.ProductResponseDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface ProductService {
    Long create(ProductRequestDto productRequestDto);
    
    Page<ProductResponseDto> getAll(Pageable pageable);


    Optional<ProductResponseDto> findById(Long id);
}
