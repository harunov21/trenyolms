package az.ingress.orderms.Service.impl;

import az.ingress.orderms.Exception.ProductFoundException;
import az.ingress.orderms.Exception.ProductNotFoundException;
import az.ingress.orderms.Model.Category;
import az.ingress.orderms.Model.Product;
import az.ingress.orderms.Repository.CategoryRepository;
import az.ingress.orderms.Repository.ProductRepo;
import az.ingress.orderms.Service.CategoryService;
import az.ingress.orderms.Service.ProductService;
import az.ingress.orderms.dto.Request.ProductRequestDto;
import az.ingress.orderms.dto.Response.CategoryResponse;
import az.ingress.orderms.dto.Response.ProductResponseDto;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {
    private final ProductRepo productRepo;
    private final ModelMapper modelMapper;
    private final CategoryRepository categoryRepository;

    public Long create(ProductRequestDto productRequestDto) {
        Long categoryId = productRequestDto.getCategoryId();
        Category category = categoryRepository.findById(categoryId)
                .orElseThrow(() -> new IllegalArgumentException("Category not found with id: " + categoryId));

        Product newProduct = Product.builder()
                .name(productRequestDto.getProductName())
                .description(productRequestDto.getDescription())
                .price(productRequestDto.getPrice())
                .quantity(productRequestDto.getQuantity())
                .category(category)
                .build();

        Product savedProduct = productRepo.save(newProduct);

        return savedProduct.getId();
    }

    @Override
    public Page<ProductResponseDto> getAll(Pageable pageable) {
        Page<Product> products = productRepo.findAll(pageable);
        return products.map(product ->ProductResponseDto.builder()
                .price(product.getPrice())
                .name(product.getName())
                .description(product.getDescription())
                .build());
    }

    @Override
    public Optional<ProductResponseDto> findById(Long id) {
        Optional<Product> product = productRepo.findById(id);
        if (product.isEmpty()){
            throw new ProductNotFoundException("Product not found");
        }
        return product.map(product1 -> ProductResponseDto.builder()
                .categoryId(product1.getCategory().getId())
                .description(product1.getDescription())
                .price(product1.getPrice())
                .build());
    }


}
