package az.ingress.orderms.Service.impl;

import az.ingress.orderms.Exception.UserNotFoundException;
import az.ingress.orderms.Model.Category;
import az.ingress.orderms.Repository.CategoryRepository;
import az.ingress.orderms.Service.CategoryService;
import az.ingress.orderms.Service.ProductService;
import az.ingress.orderms.dto.Request.CategoryRequest;
import az.ingress.orderms.dto.Response.CategoryResponse;
import az.ingress.orderms.dto.Response.ProductResponseDto;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CategoryServiceImpl implements CategoryService {
    private final CategoryRepository categoryRepository;
    private final ProductService productService;

    @Override
    @Cacheable(value = "categoriesCache", key = "'getAllCategories_' + #pageable.pageNumber")
    public Page<CategoryRequest> getAll(Pageable pageable) {
        Page<Category> all = categoryRepository.findAll(pageable);
        return all.map(category -> CategoryRequest.builder()
                .name(category.getName())
                .parentCategoryId(category.getId())
                .build());
    }
    @Override
    public Category createCategory(Category category) {
        return categoryRepository.save(category);

    }

    @Override
    @Cacheable(value = "categoriesCache", key = "'getCategoryById_' + #id")
    public Optional<CategoryResponse> getById(Long id) {
        Optional<Category> byId = categoryRepository.findById(id);
        if (byId.isEmpty()){
            throw new UserNotFoundException("Category not found");
        }
        return byId.map(category -> CategoryResponse.builder()
                .name(category.getName())
                .build());
    }

    @Override
    @Cacheable(value = "categoriesCache", key = "'getProductsByCategoryId_' + #id + '_' + #pageable.pageNumber")
    public Optional<ProductResponseDto> getProductsByCategoryId(Long id, Pageable pageable) {
        categoryRepository.findById(id).orElseThrow(()->
                new UserNotFoundException("Category not found"));
        Optional<ProductResponseDto> productById = productService.findById(id);
        return productById;

    }
    @Override
    public void deleteCategory(Long id) {

    }

}
