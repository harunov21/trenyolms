package az.ingress.orderms.Service;

import az.ingress.orderms.Model.Order;
import az.ingress.orderms.dto.Request.OrderRequestDto;
import az.ingress.orderms.dto.Response.OrderResponseDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface OrderService {
    Long createOrder(OrderRequestDto orderRequestDto);


    OrderResponseDto findById(Long id);

    Page<OrderResponseDto> getAll(Pageable pageable);
}
