package az.ingress.orderms.Service.impl;

import az.ingress.com.example.commonms.exceptions.AlreadyExistException;
import az.ingress.orderms.Exception.OperationException;
import az.ingress.orderms.Exception.ProductNotFoundException;
import az.ingress.orderms.Model.Order;
import az.ingress.orderms.Model.OrderItem;
import az.ingress.orderms.Model.Product;
import az.ingress.orderms.Repository.OrderItemRepo;
import az.ingress.orderms.Repository.ProductRepo;
import az.ingress.orderms.Service.OrderItemService;
import az.ingress.orderms.Service.OrderService;
import az.ingress.orderms.client.HelloMs;
import az.ingress.orderms.dto.Request.AddToItemRequestDto;
import az.ingress.orderms.dto.Response.OrderItemResponseDto;
import az.ingress.orderms.dto.Response.OrderResponseDto;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class OrderItemServiceImpl implements OrderItemService {
    private final OrderItemRepo orderItemRepo;
    private final ProductRepo productRepo;
    private final HelloMs helloMs;
    private final OrderService orderService;


    @Override
    public Long createOrderItem(AddToItemRequestDto addToItemRequestDto) {
        OrderItem newOrderItem = new OrderItem();

        Order order = new Order();
        order.setId(addToItemRequestDto.getOrderId());
        newOrderItem.setOrder(order);

        newOrderItem.setQuantity(addToItemRequestDto.getQuantity());
        newOrderItem.setPrice(BigDecimal.valueOf(addToItemRequestDto.getPrice()));

        OrderItem savedOrderItem = orderItemRepo.save(newOrderItem);

        return savedOrderItem.getId();
    }



    @Override
    public Page<OrderItemResponseDto> getAll(Pageable pageable) {
        Page<OrderItem> pageItem = orderItemRepo.findAll(pageable);
        return pageItem.map(orderItem -> {
            OrderItemResponseDto.OrderItemResponseDtoBuilder builder = OrderItemResponseDto.builder()
                    .price(orderItem.getPrice())
                    .quantity(orderItem.getQuantity());

            Optional<Product> productOptional = Optional.ofNullable(orderItem.getProduct());
            productOptional.ifPresent(builder::productId);

            return builder.build();
        });
    }

    @Override
    @Cacheable(value = "ordersCache" , key = "#id")
    public Optional<OrderItemResponseDto> findById(Long id) {
       Optional<OrderItem> byId = orderItemRepo.findById(id);
       if (byId.isEmpty()){
           throw new RuntimeException("Item not found");
       }
       return byId.map(orderItem -> OrderItemResponseDto.builder()
               .orderId(orderItem.getOrder())
               .price(orderItem.getPrice())
               .quantity(orderItem.getQuantity())
               .productId(orderItem.getProduct())
               .build());

    }
    @Override
    @Transactional
    public String addToItem(AddToItemRequestDto request , String authorizationHeader) {
        Boolean isUserExists = helloMs.checkUser(request.getUserId() , authorizationHeader );
        orderService.findById(request.getOrderId());

        if (isUserExists) {
            try {
                Optional<OrderItem> isOrderItemExist = orderItemRepo.findByOrder_IdAndProduct_Id(request.getOrderId(), request.getProductId().getId());

                if (isOrderItemExist.isPresent()) {
                    OrderItem item = isOrderItemExist.get();
                    int quantity = item.getQuantity();
                    item.setQuantity(quantity + 1);
                    orderItemRepo.save(item);
                } else {
                    OrderItem orderItem = new OrderItem();
                    orderItem.setPrice(BigDecimal.valueOf(request.getPrice()));
                    orderItem.setProduct(productRepo.findById(request.getProductId().getId()).orElseThrow(() -> new RuntimeException("The Product not found!")));
                    orderItem.setQuantity(1);
                    orderItemRepo.save(orderItem);
                }
            } catch (Exception e) {
                throw new OperationException("An error occurred during the operation.");
            }
        } else {
            throw new ProductNotFoundException("The product you are looking for was not found with ID: " + request.getProductId().getId());
        }
        return "OrderItem added";
    }


    }



