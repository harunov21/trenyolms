package az.ingress.orderms.Service;

import az.ingress.orderms.dto.Request.AddToItemRequestDto;
import az.ingress.orderms.dto.Response.OrderItemResponseDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface OrderItemService {
    Long createOrderItem(AddToItemRequestDto addToItemRequestDto);

    Page<OrderItemResponseDto>  getAll(Pageable pageable);

    Optional<OrderItemResponseDto> findById(Long id);

    String addToItem(AddToItemRequestDto request , String authorizationHeader);
}
