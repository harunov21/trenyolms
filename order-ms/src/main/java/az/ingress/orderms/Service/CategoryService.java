package az.ingress.orderms.Service;

import az.ingress.orderms.Model.Category;
import az.ingress.orderms.dto.Request.CategoryRequest;
import az.ingress.orderms.dto.Response.CategoryResponse;
import az.ingress.orderms.dto.Response.ProductResponseDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface CategoryService {
    Page<CategoryRequest> getAll(Pageable pageable);

    Category createCategory(Category category);

    Optional<CategoryResponse> getById(Long id);


    Optional<ProductResponseDto> getProductsByCategoryId(Long id, Pageable pageable);


    void deleteCategory(Long id);

}
