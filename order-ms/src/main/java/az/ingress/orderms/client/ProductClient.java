package az.ingress.orderms.client;

import az.ingress.orderms.dto.Response.ProductResponseDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.FeignClientProperties;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;

import static az.ingress.com.example.commonms.constants.FeignConstants.CATEGORY_SERVICE;
import static az.ingress.com.example.commonms.constants.PathConstants.API_V1_PRODUCT;


@FeignClient(value = CATEGORY_SERVICE, configuration = FeignClientProperties.FeignClientConfiguration.class)
public interface ProductClient {
    @GetMapping(API_V1_PRODUCT + "/{productId}")
    ResponseEntity<ProductResponseDto> getProductById(@PathVariable("productId") Long productId);

    @PutMapping(API_V1_PRODUCT + "/{productId}/orders/count/{increase}")
    void updateOrdersCount(@PathVariable("productId") Long productId, @PathVariable("increase") boolean increase);
}
