package az.ingress.orderms.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.FeignClientProperties;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.lang.module.Configuration;
import java.util.Optional;

import static az.ingress.com.example.commonms.constants.FeignConstants.USER_SERVICE;
import static az.ingress.com.example.commonms.constants.PathConstants.API_V1_USER;

@Component
@FeignClient(name = "user-ms")
public interface HelloMs {
    @GetMapping("api/v1/users/exists")
    Boolean checkUser(@RequestParam Long userId,
                      @RequestHeader("Authorization") String authorizationHeader);
}
