package az.ingress.userms.Service.impl;

import az.ingress.auth.config.JwtService;
import az.ingress.userms.Dto.*;
import az.ingress.userms.Enum.UserAuthority;
import az.ingress.userms.Enum.UserType;
import az.ingress.userms.Exception.PasswordException;
import az.ingress.userms.Exception.UserNotFoundException;
import az.ingress.userms.Exception.UserServiceException;
import az.ingress.userms.Model.Authority;
import az.ingress.userms.Model.Users;
import az.ingress.userms.Repository.UserRepo;
import az.ingress.userms.Service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService {

    private final UserRepo userRepo;
    private final JwtService jwtService;
    private final PasswordEncoder passwordEncoder;
    private final KafkaTemplate<String,Object > kafkaTemplate;

    @Override
    public JwtResponseDto register(RegisterDto registerDto) {

        if (!registerDto.getPassword().equals(registerDto.getRepeatedPassword())) {
            throw new PasswordException("Passwords are different");
        }
        List<Authority> authorityList = new ArrayList<>();

        registerDto.getAuthorities().forEach(s -> {
            UserAuthority userAuthority = UserAuthority.valueOf(s);
            authorityList.add(Authority.builder()
                    .userAuthority(userAuthority)
                    .build());
        });

        Users user = Users.builder()
                .name(registerDto.getName())
                .surname(registerDto.getSurname())
                .email(registerDto.getEmail())
                .password(passwordEncoder.encode(registerDto.getPassword()))
                .authorities(authorityList)
                .userType(UserType.valueOf(registerDto.getUserType()))
                .build();
        Users saveUser = userRepo.save(user);

        Set<SimpleGrantedAuthority> authorities = saveUser.getAuthorities().stream()
                .map(authority -> new SimpleGrantedAuthority(authority.getUserAuthority().name()))
                .collect(Collectors.toSet());

        UserDetails userDetails = org.springframework.security.core.userdetails.User.builder()
                .username(saveUser.getName())
                .password(passwordEncoder.encode(saveUser.getPassword()))
                .authorities(authorities)
                .build();

        kafkaTemplate.send("userTopic" , registerDto);

        String token = jwtService.generateToken((org.springframework.security.core.userdetails.User) userDetails);
        return JwtResponseDto.builder()
                .jwt(token)
                .build();
    }

    @Override
    public JwtResponseDto login(LoginDto loginDto) {
        Optional<Users> byEmail = userRepo.findByEmail(loginDto.getEmail());
        Users user = byEmail.orElseThrow(() -> new UserNotFoundException("User not found"));

        if (!passwordEncoder.matches(loginDto.getPassword(), user.getPassword())) {
            throw new PasswordException("Password does not match");
        }

        UserDetails userDetails = org.springframework.security.core.userdetails.User.builder()
                .username(user.getName())
                .password(passwordEncoder.encode(user.getPassword()))
                .build();

        String token = jwtService.generateToken((org.springframework.security.core.userdetails.User) userDetails);
        return JwtResponseDto.builder()
                .jwt(token)
                .build();
    }
    @Override
    @Cacheable(value = "usersCache", key = "'findAll' + #pageable.pageNumber")
    public Page<UserResponseDto> findAll(Pageable pageable) {
        Page<Users> users = userRepo.findAll(pageable);

        users.forEach(user -> {
            UserResponseDto userResponseDto = UserResponseDto.builder()
                    .name(user.getName())
                    .surname(user.getSurname())
                    .email(user.getEmail())
                    .userType(user.getUserType())
                    .authorities(user.getAuthorities())
                    .address(user.getAddress())
                    .build();

            kafkaTemplate.send("userTopic", userResponseDto);
        });
        return users.map(user -> UserResponseDto.builder()
                .name(user.getName())
                .surname(user.getSurname())
                .email(user.getEmail())
                .userType(user.getUserType())
                .authorities(user.getAuthorities())
                .address(user.getAddress())
                .build());
    }

    @Override
    public Page<UserResponseDto> findWithSpec(String name, String surname, String email, Pageable pageable) {
        Page<Users> users = userRepo.findWithSpec(name,surname,email,pageable);
        return users.map(users1 -> UserResponseDto.builder()
                .name(users1.getName())
                .surname(users1.getSurname())
                .email(users1.getEmail())
                .password(users1.getPassword())
                .userType(users1.getUserType())
                .authorities(users1.getAuthorities())
                .address(users1.getAddress())
                .build());
        }

    @Override
    @CacheEvict(value = "userCache" , key = "#id")
    public UserRequestDto update(UserRequestDto userRequestDto, Long id) {
        Optional<Users> optionalUser = userRepo.findById(id);

        if (optionalUser.isPresent()) {
            Users existingUser = optionalUser.get();

            existingUser.setName(userRequestDto.getName());
            existingUser.setSurname(userRequestDto.getSurname());
            existingUser.setEmail(userRequestDto.getEmail());
            existingUser.setPassword(passwordEncoder.encode(userRequestDto.getPassword()));
            existingUser.setUserType(userRequestDto.getUserType());
            existingUser.setAuthorities(userRequestDto.getAuthorities());
            existingUser.setAddress(userRequestDto.getAddress());

            userRepo.save(existingUser);

            return userRequestDto;
        } else {
            throw new UserNotFoundException("No user found with the specified ID: " + id);
        }
    }

    @Override
    public void delete(Long id) {
        userRepo.findById(id).orElseThrow(()->
                new UserNotFoundException("User not found"));
        userRepo.deleteById(id);
    }

    @Override
    public Boolean isUserExist(Long userId) {
        try {
            Optional<Users> findUser = userRepo.findById(userId);
            return findUser.isPresent();
        } catch (Exception e) {
            // Log or handle the exception based on your requirements
            throw new UserServiceException("Error checking user existence");
        }
    }

    @Override
    @Cacheable(value = "userCache" , key = "#userId")
    public Optional<UserResponseDto> findById(Long userId) {
        Optional<Users> byId = userRepo.findById(userId);
        byId.orElseThrow(() -> new UserNotFoundException("User not found"));
        return byId.map(users1 -> UserResponseDto.builder()
                .name(users1.getName())
                .surname(users1.getSurname())
                .email(users1.getEmail())
                .userType(users1.getUserType())
                .password(passwordEncoder.encode(users1.getPassword()))
                .authorities(users1.getAuthorities())
                .address(users1.getAddress())
                .build());


    }

}

