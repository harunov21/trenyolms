package az.ingress.userms.Service;

import az.ingress.userms.Dto.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface UserService {
    JwtResponseDto register(RegisterDto registerDto);

    JwtResponseDto login(LoginDto loginDto);

    Page <UserResponseDto> findAll(Pageable pageable);

    Page<UserResponseDto> findWithSpec(String name, String surname, String email, Pageable pageable);

    UserRequestDto update(UserRequestDto userRequestDto, Long id);

    void delete(Long id);

    Boolean isUserExist(Long userId);

    Optional<UserResponseDto> findById(Long userId);
}
