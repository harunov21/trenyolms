package az.ingress.userms.Exception;

import az.ingress.userms.Enum.ErrorFields;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
public class BadRequest extends RuntimeException{
    private final ErrorFields errorFields;

    public BadRequest(String message, ErrorFields errorFields) {
        super(message);
        this.errorFields = errorFields;
    }
}
