package az.ingress.userms.Exception;

public class PasswordException extends RuntimeException{
    public PasswordException(String message) {
        super(message);
    }
}
