package az.ingress.userms.Exception;

import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@ControllerAdvice
@RequiredArgsConstructor
public class GlobalExceptionHandler {
    private final MessageSource messageSource;

    @ExceptionHandler(BadRequest.class)
    public ResponseEntity<ErrorResponse> exceptionHandler(BadRequest badRequest , WebRequest webRequest){
        Map<String,String> errorFields = new HashMap<>();
        errorFields.put("error message", messageSource.getMessage(badRequest.getErrorFields().name(), null, Locale.getDefault()));
        ErrorResponse errorResponse = ErrorResponse.builder()
                .iat(LocalDateTime.now())
                .message(errorFields)
                .statusCode(400)
                .build();
        return new ResponseEntity<>(errorResponse,BAD_REQUEST);
    }

    @ExceptionHandler(UserNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<Map<String, String>> userNotFound(UserNotFoundException foundException) {
        Map<String, String> error = new HashMap<>();
        error.put("error message", foundException.getLocalizedMessage());
        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(PasswordException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public ResponseEntity<Map<String ,String >> passwordException(PasswordException passwordException){
        Map<String ,String> error = new HashMap<>();
        error.put("error message" , passwordException.getLocalizedMessage());
        return new ResponseEntity<>(error,HttpStatus.UNAUTHORIZED);

    }
    @ExceptionHandler(UserRegistrationException.class)
    @ResponseStatus(BAD_REQUEST)
    public ResponseEntity<Map<String ,String>> registrationException (UserRegistrationException userRegistrationException){
        Map<String ,String> error = new HashMap<>();
        error.put("error message" ,userRegistrationException.getLocalizedMessage());
        return new ResponseEntity<>(error , BAD_REQUEST);
    }
    @ExceptionHandler(UserServiceException.class)
    @ResponseStatus(BAD_REQUEST)
    public ResponseEntity<Map<String ,String>> userServiceException (UserServiceException e){
        Map<String ,String > error = new HashMap<>();
        error.put("error message" , e.getLocalizedMessage());
        return new ResponseEntity<>(error,BAD_REQUEST);

    }


}
