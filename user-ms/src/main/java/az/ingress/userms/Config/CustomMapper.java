package az.ingress.userms.Config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CustomMapper {

    @Bean
    public ModelMapper mapper(){
        return new ModelMapper();
    }

    }

