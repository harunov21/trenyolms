package az.ingress.userms.Model;

import az.ingress.userms.Enum.UserType;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Users implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String name;
    String surname;
    String password;
    String address;
    String email;

    @Enumerated(EnumType.STRING)
    UserType userType;

    @ManyToMany(cascade = CascadeType.ALL , fetch = FetchType.EAGER)
    @JoinTable(name = "users_authority" ,
    joinColumns = @JoinColumn(name = "users_id" , referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "authority_id" , referencedColumnName = "id"))
    List<Authority> authorities;

}
