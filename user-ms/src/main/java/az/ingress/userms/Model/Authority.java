package az.ingress.userms.Model;

import az.ingress.userms.Enum.UserAuthority;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Authority implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Enumerated(EnumType.STRING)
    UserAuthority userAuthority;

    @ManyToMany(mappedBy = "authorities")
    @JsonIgnore
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    List<Users> usersList;


}
