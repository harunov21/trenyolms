package az.ingress.userms.Dto;

import az.ingress.userms.Enum.UserType;
import az.ingress.userms.Model.Authority;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserResponseDto implements Serializable {
    Long id;
    String name;
    String surname;
    String password;
    String address;
    String email;
    UserType userType;
    List<Authority> authorities;
}
