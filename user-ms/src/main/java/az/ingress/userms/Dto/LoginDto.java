package az.ingress.userms.Dto;

import jakarta.validation.constraints.NotBlank;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class LoginDto {
    @NotBlank(message = "Xahiş olunur istifadəçi adını daxil edin!")
    private String email;
    @NotBlank(message = "Xahiş olunur parolunuzu daxil edin!")
    private String password;


}
