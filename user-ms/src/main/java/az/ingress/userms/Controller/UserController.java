package az.ingress.userms.Controller;

import az.ingress.userms.Dto.*;
import az.ingress.userms.Service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.*;


import java.util.Optional;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("/api/v1/users")
@RequiredArgsConstructor
@Slf4j
public class UserController {
    private final UserService userService;

    @Operation(summary = "Check if user exists", description = "Check if a user exists by user ID.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User exists"),
            @ApiResponse(responseCode = "500", description = "Internal server error occurred")
    })
    @GetMapping("/exists")
    public ResponseEntity<Boolean> checkUser(@RequestParam Long userId) {
        try {
            Boolean userExists = userService.isUserExist(userId);
            return new ResponseEntity<>(userExists, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error checking user existence", e);
            return new ResponseEntity<>(false, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Operation(summary = "Register a new user", description = "Register a new user.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User registered successfully"),
            @ApiResponse(responseCode = "400", description = "Invalid request format"),
            @ApiResponse(responseCode = "500", description = "Internal server error occurred")
    })
    @PostMapping("/register")
    public ResponseEntity<JwtResponseDto> register(
            @Valid @RequestBody RegisterDto registerDto) {
        return new ResponseEntity<>(userService.register(registerDto), OK);
    }

    @Operation(summary = "Login", description = "Login to the system.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User logged in successfully"),
            @ApiResponse(responseCode = "401", description = "Invalid credentials"),
            @ApiResponse(responseCode = "500", description = "Internal server error occurred")
    })
    @PostMapping("/login")
    public ResponseEntity<JwtResponseDto> login(
            @Valid @RequestBody LoginDto logInDto) {
        return new ResponseEntity<>(userService.login(logInDto), OK);
    }

    @Operation(summary = "Get all users", description = "Get a list of all users with pagination.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully retrieved the users"),
            @ApiResponse(responseCode = "500", description = "Internal server error occurred")
    })
    @GetMapping("/all")
    public Page<UserResponseDto> getAllUsers(Pageable pageable) {
        return userService.findAll(pageable);
    }

    @Operation(summary = "Find user by ID", description = "Get details of a user by user ID.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully retrieved the user"),
            @ApiResponse(responseCode = "404", description = "User not found"),
            @ApiResponse(responseCode = "500", description = "Internal server error occurred")
    })
    @GetMapping("{userId}")
    public Optional<UserResponseDto> findById(@PathVariable Long userId) {
        return userService.findById(userId);
    }

    @Operation(summary = "Find users with custom filters", description = "Get a list of users based on custom filters.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully retrieved the users"),
            @ApiResponse(responseCode = "500", description = "Internal server error occurred")
    })
    @GetMapping("/spec")
    public Page<UserResponseDto> findWithSpec(
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String surname,
            @RequestParam(required = false) String email,
            Pageable pageable) {
        return userService.findWithSpec(name, surname, email, pageable);
    }

    @Operation(summary = "Update user", description = "Update user details by user ID.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User updated successfully"),
            @ApiResponse(responseCode = "400", description = "Invalid request format"),
            @ApiResponse(responseCode = "404", description = "User not found"),
            @ApiResponse(responseCode = "500", description = "Internal server error occurred")
    })
    @PutMapping("/{id}")
    public ResponseEntity<UserRequestDto> updateUser(
            @Valid @RequestBody UserRequestDto userRequestDto,
            @Parameter(description = "User ID", required = true)
            @PathVariable Long id) {
        return new ResponseEntity<>(userService.update(userRequestDto,id) , OK);

    }


    @Operation(summary = "Delete user", description = "Delete user by user ID.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User deleted successfully"),
            @ApiResponse(responseCode = "404", description = "User not found"),
            @ApiResponse(responseCode = "500", description = "Internal server error occurred")
    })
    @DeleteMapping("/{id}")
    public void deleteUser(
            @Parameter(description = "User ID", required = true)
            @PathVariable Long id) {
        userService.delete(id);
    }
}
